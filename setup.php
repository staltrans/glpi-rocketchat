<?php
/*
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2017 by the RocketChat Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_ROCKETCHAT_VERSION', '0.8');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_rocketchat() {

   global $PLUGIN_HOOKS;

   $plug = new Plugin();

   Plugin::registerClass('PluginRocketchatProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['csrf_compliant']['rocketchat'] = true;

   $PLUGIN_HOOKS['menu_toadd']['rocketchat'] = [
      'plugins' => 'PluginRocketchatWebhook',
   ];

   $PLUGIN_HOOKS['item_add']['rocketchat'] = [
      // Parent class CommonITILObject
      'Ticket'             => ['PluginRocketchatHook', 'itemAddITILObject'],
      'Problem'            => ['PluginRocketchatHook', 'itemAddITILObject'],
      'Change'             => ['PluginRocketchatHook', 'itemAddITILObject'],
      // Parent class CommonITILTask
      'TicketTask'         => ['PluginRocketchatHook', 'itemAddITILTask'],
      'ProblemTask'        => ['PluginRocketchatHook', 'itemAddITILTask'],
      'ChangeTask'         => ['PluginRocketchatHook', 'itemAddITILTask'],
      // Parent class CommonITILActor
      'Ticket_User'        => ['PluginRocketchatHook', 'itemAddITILActor'],
      'Problem_User'       => ['PluginRocketchatHook', 'itemAddITILActor'],
      'Change_User'        => ['PluginRocketchatHook', 'itemAddITILActor'],
      'ITILSolution'       => ['PluginRocketchatHook', 'itemAddITILSolution'],
      // For Ticket
      'TicketFollowup'     => ['PluginRocketchatHook', 'itemAddTicketFollowup'],
      // For Change, Problem
      'Document_Item'      => ['PluginRocketchatHook', 'itemAddDocumentItem'],

      'Notepad'            => ['PluginRocketchatHook', 'itemAddNotepad'],

      'TicketValidation'   => ['PluginRocketchatHook', 'itemAddCommonITILValidation'],
      'ChangeValidation'   => ['PluginRocketchatHook', 'itemAddCommonITILValidation'],
   ];

   $PLUGIN_HOOKS['item_update']['rocketchat'] = [
      // Parent class CommonITILObject
      'Ticket'             => ['PluginRocketchatHook', 'itemUpdateITILObject'],
      'Problem'            => ['PluginRocketchatHook', 'itemUpdateITILObject'],
      'Change'             => ['PluginRocketchatHook', 'itemUpdateITILObject'],
      // Parent class CommonITILTask
      'TicketTask'         => ['PluginRocketchatHook', 'itemUpdateITILTask'],
      'ProblemTask'        => ['PluginRocketchatHook', 'itemUpdateITILTask'],
      'ChangeTask'         => ['PluginRocketchatHook', 'itemUpdateITILTask'],

      'TicketSatisfaction' => ['PluginRocketchatHook', 'itemUpdateTicketSatisfaction'],

      'TicketValidation'   => ['PluginRocketchatHook', 'itemUpdateCommonITILValidation'],
      'ChangeValidation'   => ['PluginRocketchatHook', 'itemUpdateCommonITILValidation'],
   ];

   if ($plug->isActivated('itemrating')) {
      $PLUGIN_HOOKS['item_update']['rocketchat']['PluginItemratingRating'] = ['PluginRocketchatHook', 'itemUpdateRating'];
   }

   $PLUGIN_HOOKS['item_delete']['rocketchat'] = [
      // Parent class CommonITILObject
      'Ticket'         => ['PluginRocketchatHook', 'itemDeleteITILObject'],
      'Problem'        => ['PluginRocketchatHook', 'itemDeleteITILObject'],
      'Change'         => ['PluginRocketchatHook', 'itemDeleteITILObject'],
   ];

   $PLUGIN_HOOKS['item_restore']['rocketchat'] = [
      'Ticket'         => ['PluginRocketchatHook', 'itemRestoreITILObject'],
      'Problem'        => ['PluginRocketchatHook', 'itemRestoreITILObject'],
      'Change'         => ['PluginRocketchatHook', 'itemRestoreITILObject'],
   ];

   $PLUGIN_HOOKS['item_purge']['rocketchat'] = [
      // Parent class CommonITILActor
      'Ticket_User'    => ['PluginRocketchatHook', 'itemPurgeITILActor'],
      'Problem_User'   => ['PluginRocketchatHook', 'itemPurgeITILActor'],
      'Change_User'    => ['PluginRocketchatHook', 'itemPurgeITILActor'],
   ];

   $PLUGIN_HOOKS['retrieve_more_data_from_ldap']['rocketchat'] = ['PluginRocketchatHook', 'usersRelationships'];

   $PLUGIN_HOOKS['config_page']['rocketchat'] = 'front/config.form.php';
}

/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_rocketchat() {
   return [
      'name'           => 'RocketChat',
      'version'        => PLUGIN_ROCKETCHAT_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-rocketchat',
      'minGlpiVersion' => '9.3'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_rocketchat_check_prerequisites() {
   $plugin = new Plugin();
   if (!$plugin->isInstalled('config')) {
      printf(__('This plugin requires %s', 'rocketchat'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('config')) {
      printf(__('Please activate %s', 'rocketchat'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isInstalled('libraries')) {
      printf(__('This plugin requires %s', 'rocketchat'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('libraries')) {
      printf(__('Please activate %s', 'rockatchat'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!class_exists('\RocketChat\RocketChat')) {
      printf(__('Please install %s', 'rocketchat'), '<a href="' . PluginLibrariesList::getURL() . '">staltrans/rocket-chat-rest-client</a>');
      return false;
   }

   // Strict version check (could be less strict, or could allow various version)
   if (version_compare(GLPI_VERSION, '9.3', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.3');
      } else {
         echo "This plugin requires GLPI >= 9.3";
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_rocketchat_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'rocketchat');
   }
   return false;
}

function plugin_rocketchat_libraries_composer_help() {
   return [
      'title' => sprintf(__('Install %s', 'rockatchat'), 'staltrans/rocket-chat-rest-client'),
      'content' => '[... how to install staltrans/rocket-chat-rest-client ...]'
   ];
}
