<?php
/*
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2017 by the RocketChat Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_rocketchat_install() {

   global $DB;

   $t_chats    = 'glpi_plugin_rocketchat_chats_items';
   $t_visitors = 'glpi_plugin_rocketchat_users_visitors';
   $t_config   = 'glpi_plugin_rocketchat_config';
   $t_webhooks = 'glpi_plugin_rocketchat_webhooks';

   if (TableExists($t_config)) {
       $query = "DROP TABLE `$t_config`";
       $DB->queryOrDie($query);
   }

   if (!TableExists($t_chats)) {
      $query = "CREATE TABLE `$t_chats` (
                  `channel_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                  `items_id` int(11) NOT NULL,
                  `itemtype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
                  UNIQUE KEY `iid` (`itemtype`,`items_id`),
                  UNIQUE KEY `cid` (`channel_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_visitors)) {
      $query = "CREATE TABLE `$t_visitors` (
                  `visitors_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                  `visitors_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `guid` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                  UNIQUE KEY `visitors_name` (`visitors_name`),
                  UNIQUE KEY `guid` (`guid`),
                  KEY `vid` (`visitors_id`),
                  KEY `users_id` (`users_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_webhooks)) {
      $query = "CREATE TABLE `$t_webhooks` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `entities_id` int(11) NOT NULL DEFAULT '0',
                  `is_recursive` tinyint(1) NOT NULL DEFAULT '0',
                  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  `url` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `event` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `channel_type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `name` (`name`),
                  KEY `entities_id` (`entities_id`),
                  KEY `is_recursive` (`is_recursive`),
                  KEY `is_deleted` (`is_deleted`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  UNIQUE KEY `event_channel_type` (`event`, `channel_type`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (TableExists($t_webhooks)) {

      $query = "ALTER TABLE `$t_webhooks` DROP KEY `event_channel_type`";
      $DB->query($query);
      $query = "ALTER TABLE `$t_webhooks` DROP KEY `url_token`";
      $DB->query($query);
      $query = "ALTER TABLE `$t_webhooks` DROP KEY `event`";
      $DB->query($query);
      $query = "ALTER TABLE `$t_webhooks` DROP KEY `channel_type`";
      $DB->query($query);
      $query = "ALTER TABLE `$t_webhooks` DROP KEY `url`";
      $DB->query($query);

      $query = "ALTER TABLE `$t_webhooks` ADD UNIQUE KEY `url_token` (`url`(223),`token`(32))";
      $DB->queryOrDie($query);
      $query = "ALTER TABLE `$t_webhooks` ADD KEY `event` (`event`)";
      $DB->queryOrDie($query);
      $query = "ALTER TABLE `$t_webhooks` ADD KEY `channel_type` (`channel_type`)";
      $DB->queryOrDie($query);
      $query = "ALTER TABLE `$t_webhooks` ADD KEY `url` (`url`(255))";
      $DB->queryOrDie($query);

   }

   $taskopt = [
      'allowmode' => CronTask::MODE_EXTERNAL,
      'mode'      => CronTask::MODE_EXTERNAL,
   ];

   CronTask::Register('PluginRocketchatCron', 'UsersFetch', 17 * MINUTE_TIMESTAMP, $taskopt);
   CronTask::Register('PluginRocketchatCron', 'CheckLogin', 51 * MINUTE_TIMESTAMP, $taskopt);
   CronTask::Register('PluginRocketchatCron', 'ChannelKickAll', DAY_TIMESTAMP, $taskopt);

   $req_type = new RequestType();
   $input = ['name' => 'Rocket.Chat+'];
   $input = $req_type->prepareInputForAdd($input);

   $cfg = new PluginRocketchatConfig();
   if ($req_id = $req_type->add($input)) {
      $settings = $cfg->get($cfg::VAR_SETTINGS);
      $settings->requesttypes_id = $req_id;
      $cfg->set($cfg::VAR_SETTINGS, $settings);
   }
   $cfg->del('web_hooks');

   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_rocketchat_uninstall() {

   global $DB;

   $tables = [
      'glpi_plugin_rocketchat_chats_items',
      'glpi_plugin_rocketchat_users_visitors',
      'glpi_plugin_rocketchat_config',
      'glpi_plugin_rocketchat_webhooks'
   ];

   foreach ($tables as $table) {
      if (TableExists($table)) {
          $query = "DROP TABLE `$table`";
          $DB->queryOrDie($query);
      }
   }
   $cfg = new PluginRocketchatConfig();
   $cfg->delAll();

   $cli = new PluginRocketchatClient();
   $cli->delAll();

   return true;
}
