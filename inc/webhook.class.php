<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatWebhook extends CommonDBTM {

   // From CommonDBTM
   public $dohistory         = true;
   public $history_blacklist = ['date_mod'];

   // Actions
   const SEND_MESSAGE  = 'send_message';
   const FILE_UPLOADED = 'file_uploaded';
   const ROOM_ARCHIVED = 'room_archived';
   const ROOM_CREATED  = 'room_created';
   const ROOM_JOINED   = 'room_joined';
   const ROOM_LEFT     = 'room_left';
   const USER_CREATED  = 'user_created';

   // Channel types
   const PUBLIC_CHANNELS = 'all_public_channels';
   const PRIVATE_GROUPS  = 'all_private_groups';
   const DIRECT_MESSAGES = 'all_direct_messages';

   static $rightname = 'plugin_rocketchat_webhook';

   static function getTypeName($nb = 0) {
      return PluginRocketchatTr::__('Интергация с Rocket.Chat');
   }

   static function getEvents($key = '') {
      $actions = [
         self::SEND_MESSAGE  => PluginRocketchatTr::__('Send message'),
         self::FILE_UPLOADED => PluginRocketchatTr::__('File uploaded'),
         self::ROOM_ARCHIVED => PluginRocketchatTr::__('Room archived'),
         self::ROOM_CREATED  => PluginRocketchatTr::__('Room created'),
         self::ROOM_JOINED   => PluginRocketchatTr::__('Room joined'),
         self::ROOM_LEFT     => PluginRocketchatTr::__('Room left'),
         self::USER_CREATED  => PluginRocketchatTr::__('User created'),
      ];
      if (empty($key)) {
         return $actions;
      } else if (isset($actions[$key])) {
         return $actions[$key];
      }
      return false;
   }

   static function getChannelTypes($key = '') {
      $types = [
         self::PUBLIC_CHANNELS => PluginRocketchatTr::__('All public channels'),
         self::PRIVATE_GROUPS  => PluginRocketchatTr::__('All private groups'),
         self::DIRECT_MESSAGES => PluginRocketchatTr::__('All direct messages'),
      ];
      if (empty($key)) {
         return $types;
      } else if (isset($types[$key])) {
         return $types[$key];
      }
      return false;
   }

   static function getAdditionalMenuLinks() {
      global $CFG_GLPI;
      $opt['config'] = PluginRocketchatConfig::getFormURL();

      $img = Html::image(
         $CFG_GLPI['root_doc'] . '/plugins/rocketchat/img/icon-mono-21x18.png',
         ['alt' => PluginRocketchatTr::__('Аккаунт бота в Rocket.Chat')]
      );
      $opt[$img] = PluginRocketchatClient::getFormURL();

      $opt += PluginRocketchatDebug::getDebugMenuLink();

      return $opt;
   }

   function rawSearchOptions() {

      $tab = [];

      $tab[] = [
         'id' => 'common',
         'name' => PluginRocketchatTr::__('Rocket.Chat webhooks'),
      ];

      $i = 1;

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'name',
         'name' => PluginRocketchatTr::__('Title'),
         'datatype' => 'itemlink',
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'url',
         'name' => PluginRocketchatTr::__('URL'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'token',
         'name' => PluginRocketchatTr::__('Token'),
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'event',
         'name' => PluginRocketchatTr::__('Event'),
         'searchtype' => 'equals',
         'datatype' => 'specific',
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'channel_type',
         'name' => PluginRocketchatTr::__('Channel type'),
         'searchtype' => 'equals',
         'datatype' => 'specific',
      ];

      return $tab;
   }

   /**
    *
    */
   static function getSpecificValueToDisplay($field, $values, array $options = []) {

      if (!is_array($values)) {
         $values = [$field => $values];
      }
      switch ($field) {
         case 'event':
            return self::getEvents($values[$field]);

         case 'channel_type':
            return self::getChannelTypes($values[$field]);
      }
      return parent::getSpecificValueToDisplay($field, $values, $options);

   }

   /**
    *
    */
   static function getSpecificValueToSelect($field, $name = '', $values = '', array $options = []) {

      if (!is_array($values)) {
         $values = [$field => $values];
      }
      $options['display'] = false;

      switch ($field) {

         case 'event':
            $options['name']  = $name;
            $options['value'] = $values[$field];
            return self::dropdownEvent($options);

         case 'channel_type':
            $options['name']  = $name;
            $options['value'] = $values[$field];
            return self::dropdownChannelType($options);

      }
      return parent::getSpecificValueToSelect($field, $name, $values, $options);
   }


   /**
    *
    */
   static function dropdownEvent(array $options = []) {

      $p = [
         'name'     => 'event',
         'value'    => '',
         'showtype' => 'normal',
         'display'  => true,
      ];

      if (is_array($options) && count($options)) {
         foreach ($options as $key => $val) {
            $p[$key] = $val;
         }
      }

      return Dropdown::showFromArray($p['name'], self::getEvents(), $p);

   }

   /**
    *
    */
   static function dropdownChannelType(array $options = []) {

      $p = [
         'name'     => 'channel_type',
         'value'    => '',
         'showtype' => 'normal',
         'display'  => true,
      ];

      if (is_array($options) && count($options)) {
         foreach ($options as $key => $val) {
            $p[$key] = $val;
         }
      }

      return Dropdown::showFromArray($p['name'], self::getChannelTypes(), $p);

   }

   function defineTabs($options = []) {

      $ong = [];
      $this->addDefaultFormTab($ong);
      $this->addStandardTab('Log', $ong, $options);
      return $ong;

   }

   /**
    *
    */
   function getSpecificMassiveActions($checkitem = null) {

      $isadmin = self::canUpdate();
      $actions = parent::getSpecificMassiveActions($checkitem);
      if ($isadmin) {
         $my_actions = [
            'gen_url' => PluginRocketchatTr::__('Generate URL'),
            'gen_token' => PluginRocketchatTr::__('Generate token'),
         ];
         foreach ($my_actions as $key => $description) {
            $actions[__CLASS__ . MassiveAction::CLASS_ACTION_SEPARATOR . $key] = $description;
         }
         MassiveAction::getAddTransferList($actions);
      }
      return $actions;
   }

   static function processMassiveActionsForOneItemtype(MassiveAction $ma, CommonDBTM $item, array $ids) {
      $action = $ma->getAction();
      if ($action == 'gen_url' || $action == 'gen_token') {
         foreach ($ids as $id) {
            if ($item->getFromDB($id)) {
               if ($action == 'gen_url') {
                  $item->fields['url'] = $item->genUrl();
               }
               if ($action == 'gen_token') {
                  $item->fields['token'] = $item->genString();
               }
               if ($item->update($item->fields)) {
                  $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_OK);
               } else {
                  $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_KO);
               }
            } else {
               $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_KO);
            }
         }
         return;
      }

      parent::processMassiveActionsForOneItemtype($ma, $item, $ids);
   }

   function genString($length = 32) {
      $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
      $count = mb_strlen($chars);
      for ($i = 0, $out = ''; $i < $length; $i++) {
         $index = rand(0, $count - 1);
         $out .= mb_substr($chars, $index, 1);
      }
      return $out;
   }

   function genUrl($length = 32) {

      global $CFG_GLPI;
      //$prefix = $CFG_GLPI['url_base'] . $this->getFormURL();
      $prefix = $CFG_GLPI['url_base'] . Toolbox::getItemTypeFormURL('PluginRocketchatIncoming', true);
      return $prefix . '/' . $this->genString($length);

   }

   function showForm($ID, $options = []) {

      $this->initForm($ID, $options);
      $this->showFormHeader($options);
      echo '<tr><td>' .PluginRocketchatTr::__('Name') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'name');
      echo '</td><td></td><td></td></tr>';
      echo '<tr>';
      echo '<td>' .PluginRocketchatTr::__('URL') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'url', ['option' => 'readonly="readonly"']);
      echo '</td>';
      echo '<td>' .PluginRocketchatTr::__('Token') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'token');
      echo '</td>';
      echo'</tr>';
      if (!empty($ID)) {
         echo '<tr><td></td><td>';
         echo Html::submit(_sx('button', 'Generate URL'), ['name' => 'generate_url']);
         echo '</td><td></td><td>';
         echo Html::submit(_sx('button', 'Generate Token'), ['name' => 'generate_token']);
         echo '</td></tr>';
      }
      echo '<tr>';
      echo '<td>' .PluginRocketchatTr::__('Event') . '</td>';
      echo '<td>';
      echo self::dropdownEvent(['display' => false, 'value' => $this->fields['event']]);
      echo '</td>';
      echo '<td>' .PluginRocketchatTr::__('Channel type') . '</td>';
      echo '<td>';
      echo self::dropdownChannelType(['display' => false, 'value' => $this->fields['channel_type']]);
      echo '</td>';
      echo'</tr>';
      $this->showFormButtons($options);

      return true;
   }

   function post_getEmpty() {
      $this->fields['url'] = $this->genUrl();
   }

   function prepareInputForAdd($input) {
      if (empty($input['name']) && !empty($input['event'])) {
         $input['name'] = sprintf(PluginRocketchatTr::__('Webhook for event: %s'), self::getEvents($input['event']));
      }

      return $input;
   }

   function prepareInputForUpdate($input) {
      if (empty($input['name']) && !empty($input['event'])) {
         $input['name'] = sprintf(PluginRocketchatTr::__('Webhook for event: %s'), self::getEvents($input['event']));
      }
      if (empty($input['url'])) {
         $input['url'] = $this->genUrl();
      }

      return $input;
   }

   function save($post = []) {
      if (isset($post['add'])) {
         $this->check(-1, CREATE, $post);
         $this->add($post);
      } else if (isset($post['delete'])) {
         $this->check($post['id'], DELETE);
         $this->delete($post);
         $this->redirectToList();
      } else if (isset($post['restore'])) {
         $this->check($post['id'], PURGE);
         $this->restore($post);
         $this->redirectToList();
      } else if (isset($post['purge'])) {
         $this->check($post['id'], PURGE);
         $this->delete($post, 1);
         $this->redirectToList();
      } else if (isset($post['update'])) {
         $this->check($post['id'], UPDATE);
         $this->update($post);
      } else if (isset($post['generate_url'])) {
         $this->check($post['id'], UPDATE);
         $post['url'] = $this->genUrl();
         $this->update($post);
      } else if (isset($post['generate_token'])) {
         $this->check($post['id'], UPDATE);
         $post['token'] = $this->genString();
         $this->update($post);
      }

   }

}
