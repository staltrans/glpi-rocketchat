<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatClient extends PluginConfigVariable {

   const VAR_BOT_ACCOUNT = 'bot_account';
   const VAR_USER_ID     = 'user_id';
   const VAR_AUTH_TOKEN  = 'auth_token';
   const VAR_BOT_INFO    = 'bot_info';
   const PLUGIN          = 'rocketchat';

   //static $rightname      = 'plugin_rocketchat_config';
   protected $displaylist = false;

   private $user_id;
   private $auth_token;
   private $bot_account;


   static function getTypeName($nb = 0) {
      return PluginRocketchatTr::__('Аккаунт бота Rocket.Chat');
   }

   static function getMenuName() {
      return PluginRocketchatTr::__('Аккаунт бота Rocket.Chat');
   }

   function defaultValue($variable) {
      switch ($variable) {
         case self::VAR_BOT_ACCOUNT:
            return (object) [
               'username'    => 'rocket.cat',
               'password'    => '',
               'server'      => '',
               'name_regexp' => '[0-9a-zA-Z-_.]+',
            ];
         default:
            return false;
      }
   }

   function showFormMain() {

      $val = $this->defaultValue(self::VAR_BOT_ACCOUNT);
      if (!empty($this->fields['value'])) {
         $val = $this->fields['value'];
      }

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . __('Login') . '</td>';
      echo '<td>';
      echo '<input type="hidden" name="name" value="' . self::VAR_BOT_ACCOUNT . '">';
      echo '<input type="hidden" name="plugin" value="' .  self::PLUGIN . '">';
      echo '<input type="text" name="value[username]" size="60" value="' . $val->username . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . __('Password') . '</td>';
      echo '<td><input type="password" name="value[password]" size="60" value="' . $val->password . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . __('Server') . '</td>';
      echo '<td><input type="text" name="value[server]" size="60" value="' .  $val->server . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginRocketchatTr::__('Регулярное выражение для проверки имён') . '</td>';
      echo '<td><input type="text" name="value[name_regexp]" size="60" value="' . $val->name_regexp . '"></td>';
      echo '</tr>';

   }

   function defineTabs($options = []) {
      $ong = parent::defineTabs($options);
      $this->addStandardTab(__CLASS__, $ong, $options);
      return $ong;
   }

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == __CLASS__) {
            $tabs[1] = PluginRocketchatTr::__('Проверка');
            return $tabs;
      }
      return '';
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {
      if ($item->getType() == __CLASS__ && $tabnum == 1) {
         $item->showFormChecking();
      }
      return true;
   }

   function showFormChecking() {
      $vals = $this->get(self::VAR_BOT_INFO);
      echo '<div class="center" id="tabsbody">';
      echo '<form name="form" action="' . Toolbox::getItemTypeFormURL(__CLASS__) . '" method="post">';
      echo '<table class="tab_cadre_fixe">';
      echo '<tr class="tab_bg_2">';
      echo '<th colspan="2" class="center">' . PluginRocketchatTr::__('Auth info') . '</th>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right"><strong>' . PluginRocketchatTr::__('User ID') . ':</strong></td>';
      echo '<td class="left">';
      echo $this->getUserId();
      echo '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right"><strong>' . PluginRocketchatTr::__('Auth Token') . ':</strong></td>';
      echo '<td class="left">';
      echo $this->getAuthToken();
      echo '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<th colspan="2" class="center">' . PluginRocketchatTr::__('About bot') . '</th>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('ID') . ':</td>';
      echo '<td class="left">' . (isset($vals->_id) ? $vals->_id : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Имя') . ':</td>';
      echo '<td class="left">' . (isset($vals->name) ? $vals->name : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Email адрес') . ':</td>';
      echo '<td class="left">' .
         (isset($vals->emails[0]->address) ? $vals->emails[0]->address : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Email verified') . ':</td>';
      echo '<td class="left">' .
         (isset($vals->emails[0]->verified) ? $vals->emails[0]->verified : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Статус') . ':</td>';
      echo '<td class="left">' . (isset($vals->status) ? $vals->status : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Status connection') . ':</td>';
      echo '<td class="left">' .
         (isset($vals->statusConnection) ? $vals->statusConnection : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . __('Login') . ':</td>';
      echo '<td class="left">' . (isset($vals->username) ? $vals->username : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('UTC offset') . ':</td>';
      echo '<td class="left">' . (isset($vals->utcOffset) ? $vals->utcOffset : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Active') . ':</td>';
      echo '<td class="left">' . (isset($vals->active) ? $vals->active : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td class="right">' . PluginRocketchatTr::__('Success') . ':</td>';
      echo '<td class="left">' . (isset($vals->success) ? $vals->success : '') . '</td>';
      echo '</tr>';
      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2" class="center">';
      echo Html::submit(PluginRocketchatTr::_sx('button', 'Проверить'), ['name' => 'check_login']);
      echo '</td>';
      echo '</tr>';
      echo '</table>';
      Html::closeForm();
      echo '</div>';

   }

   function save($post = []) {
      if (isset($post['check_login'])) {
         $about = $this->checkMe();
         if (!empty($about)) {
            $this->set(self::VAR_BOT_INFO, $about);
         }
      } else {
         parent::save($post);
      }
   }

   private function log($event = '', $level = 4) {
      Event::log(-1, strtolower(get_class($this)), $level, 'rocketchat', $event);
   }

   private function httpLog($resp, $event = '', $level = 4) {
      if (isset($resp->code)) {
         $event .= ' ' . sprintf(PluginRocketchatTr::__('Код ответа HTTP %s'), $resp->code);
      }
      if (isset($resp->body->error)) {
         $event .= " {$resp->body->error}.";
      }
      $this->log($event, $level);
   }

   private function is_ok($resp) {
      return ($resp->code == 200 && isset($resp->body->success) && $resp->body->success);
   }

   private function validName($name, $delimiter = '-') {
      if (preg_match_all("/{$this->bot_account->name_regexp}/u", $name, $match) > 0) {
         $search = ['/-+/u', '/_+/u', '/\.+/u'];
         $replace = ['-', '_', '.'];
         return preg_replace($search, $replace, join($delimiter, $match[0]));
      }
      return false;
   }

   private function prepareChat() {
      $cli = $this->getBotAccount();
      if (!empty($cli)) {
         $chat = new \RocketChat\RocketChat($cli->server);
         $chat->setUserId($this->getUserId());
         $chat->setAuthToken($this->getAuthToken());
         return $chat;
      }
      return false;
   }

   function getBotAccount() {
      if (empty($this->bot_account)) {
         $this->bot_account = $this->get(self::VAR_BOT_ACCOUNT);
      }
      return $this->bot_account;
   }

   function getUserId() {
      if (empty($this->user_id)) {
         $this->user_id = $this->get(self::VAR_USER_ID);
      }
      return $this->user_id;
   }

   function getAuthToken() {
      if (empty($this->auth_token)) {
         $this->auth_token = $this->get(self::VAR_AUTH_TOKEN);
      }
      return $this->auth_token;
   }

   function setUserId($user_id) {
      $this->set(self::VAR_USER_ID, $user_id);
      $this->user_id = $user_id;
   }

   function setAuthToken($auth_token) {
      $this->set(self::VAR_AUTH_TOKEN, $auth_token);
      $this->auth_token = $auth_token;
   }

   function login() {
      $cli = $this->getBotAccount();
      if (!empty($cli)) {
         $chat = new \RocketChat\RocketChat($cli->server);
         try {
            $resp = $chat->login($cli->username, $cli->password);
            $user_id = $chat->getUserId();
            $auth_token = $chat->getAuthToken();
            if (!empty($user_id) && !empty($auth_token)) {
               $this->setUserId($user_id);
               $this->setAuthToken($auth_token);
               return true;
            } else {
               $msg = sprintf(PluginRocketchatTr::__('Ошибка авторизации на сервере %s'), $cli->server) . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function me() {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->me();
            if ($this->is_ok($resp)) {
               return $resp->body;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка получения информации о боте') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function checkMe() {
      $about = $this->me();
      if (!empty($about)) {
         return $about;
      } else {
         if ($this->login()) {
            $about = $this->me();
            if (!empty($about)) {
               return $about;
            }
         }
      }
      return false;
   }

   function usersList() {
      if ($chat = $this->prepareChat()) {
         try {
            $step = 50;
            $offset = 0;
            $list = [];
            $cnt = $step;
            while ($cnt == $step) {
               $resp = $chat->usersList(["offset=$offset", "count=$step"]);
               if ($this->is_ok($resp)) {
                  $tmp = $resp->body->users;
                  $offset += $step;
                  $cnt = count($tmp);
                  $list = array_merge($list, $tmp);
               } else {
                  $msg = PluginRocketchatTr::__('Ошибка получения пользователей из Rocket.Chat') . '.';
                  $this->httpLog($resp, $msg, 1);
                  break;
               }
            }
            if (!empty($list)) {
               return $list;
            } else {
               return false;
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsCreate($name, $uids = []) {
      if ($chat = $this->prepareChat()) {
         $members = $this->getVisitorsNames($uids);
         try {
            if ($name = $this->validName($name)) {
               $resp = $chat->groupsCreate([
                  'name'    => $name,
                  'members' => $members
               ]);
               if ($this->is_ok($resp)) {
                  return isset($resp->body->group->_id) ? $resp->body->group->_id : false;
               } else if ($resp->code == 400) {
                  $msg = PluginRocketchatTr::__('Ошибка при создании группы') . '.';
                  $this->httpLog($resp, $msg, 1);
                  if (isset($resp->body->errorType) && $resp->body->errorType == 'error-duplicate-channel-name') {
                     $gr_info = $chat->groupsInfo(null, $name);
                     if ($this->is_ok($gr_info)) {
                        $group_id = $gr_info->body->group->_id;
                        foreach ($this->getVisitorsIds($uids) as $vid) {
                           $this->groupsInvite($group_id, $vid);
                        }
                        return $group_id;
                     } else {
                        $msg = PluginRocketchatTr::__('Ошибка при запросе информации о группе') . '.';
                        $this->httpLog($gr_info, $msg);
                     }
                  }
               } else {
                  $msg = PluginRocketchatTr::__('Ошибка при создании группы') . '.';
                  $this->httpLog($resp, $msg, 1);
               }
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsRename($room_id, $newname) {
      if ($chat = $this->prepareChat()) {
         try {
            if ($newname = $this->validName($newname)) {
               $resp = $chat->groupsRename($room_id, $newname);
               if ($this->is_ok($resp)) {
                  return $resp->body->group->_id;
               } else {
                  $msg = PluginRocketchatTr::__('Ошибка переименования группы') . '.';
                  $this->httpLog($resp, $msg, 1);
               }
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsSetTopic($room_id, $topic) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsSetTopic($room_id, $topic);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка изменения топика') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsSetDescription($room_id, $description) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsSetDescription($room_id, $description);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка измеенния описания') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsAddLeader($room_id, $user_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsAddLeader($room_id, $user_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = sprintf(PluginRocketchatTr::__('Не удалось сделать пользователя %s лидером канала %s'), $user_id, $room_id) . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsRemoveLeader($room_id, $user_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsRemoveLeader($room_id, $user_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = sprintf(PluginRocketchatTr::__('Не удалось снять с пользователя %s права лидера канала %s'), $user_id, $room_id) . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsSetReadOnly($room_id, $ro = true) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsSetReadOnly($room_id, $ro);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = sprintf(PluginRocketchatTr::__('Не удалось сменить флаг "только для чтения" у канала %s'), $room_id) . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   private function chatPostMessage($data) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->chatPostMessage($data);
            if ($this->is_ok($resp)) {
               return $resp->body->message->_id;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка отправки сообщения') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function chatSendMessage($chat, $message) {
      global $CFG_GLPI;
      $data = [
         'alias' => 'GLPI',
         'avatar' => $CFG_GLPI['url_base'] . '/plugins/rocketchat/images/logo-50.png',
      ];
      if (is_array($message)) {
         $data = array_merge($data, $message);
      } else {
         $data += ['text' => $message];
      }
      if ($chat[0] == '@' || $chat[0] == '#') {
         $data['channel'] = $chat;
      } else {
         $data['roomId'] = $chat;
      }
      return $this->chatPostMessage($data);
   }

   function chatPinMessage($message_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->chatPinMessage($message_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Сообщение не прикрепленно') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsArchive($room_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsArchive($room_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка архивации группы') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsUnarchive($room_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsUnarchive($room_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка извлечения из архива группы') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsKick($room_id, $user_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsKick($room_id, $user_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка иcключения из группы') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function groupsInvite($room_id, $user_id) {
      if ($chat = $this->prepareChat()) {
         try {
            $resp = $chat->groupsInvite($room_id, $user_id);
            if ($this->is_ok($resp)) {
               return true;
            } else {
               $msg = PluginRocketchatTr::__('Ошибка добавления пользователя в канал') . '.';
               $this->httpLog($resp, $msg, 1);
            }
         } catch (Exception $e) {
            $this->log(__('Error') . ': ' . $e->getMessage(), 1);
         }
      }
      return false;
   }

   function getVisitorsIds($uids) {
      return $this->getVisitors($uids, 'visitors_id');
   }

   function getVisitorsNames($uids) {
      return $this->getVisitors($uids, 'visitors_name');
   }

   private function getVisitors($uids, $fieldname) {
      global $DB;
      $ids = implode(',', $uids);
      if (!empty($ids)) {
         $t_visitors = 'glpi_plugin_rocketchat_users_visitors';
         $q = "SELECT `$fieldname`
                  FROM `$t_visitors`
               WHERE `users_id` IN ($ids)";
         if ($result = $DB->query($q)) {
            $vals = [];
            while ($data = $DB->fetch_assoc($result)) {
               if (!empty($data[$fieldname])) {
                  $vals []= $data[$fieldname];
               }
            }
            return $vals;
         }
      }
      return [];
   }

   function getChannelId($item_id, $item_type) {
      global $DB;
      $t_chats = 'glpi_plugin_rocketchat_chats_items';
      $q = "SELECT `channel_id`
               FROM `$t_chats`
            WHERE `items_id` = '$item_id' AND `itemtype` = '$item_type'";
      if (($result = $DB->query($q)) && ($DB->numrows($result) == 1)) {
         $data = $DB->fetch_assoc($result);
         return $data['channel_id'];
      }
      return false;
   }

   function delChannelId($room_id) {
      global $DB;
      $t_chats = 'glpi_plugin_rocketchat_chats_items';
      $q = "DELETE FROM `$t_chats`
            WHERE `channel_id` = '$room_id'";
      $DB->query($q);
   }

   function usersRelationshipsUpdateUID($guid, $uid) {
      global $DB;
      $t_visitors = 'glpi_plugin_rocketchat_users_visitors';
      $q = "SELECT *
            FROM `$t_visitors`
            WHERE `guid` = '$guid'";
      if (($result = $DB->query($q)) && ($DB->numrows($result) == 1)) {
         $q = "UPDATE `$t_visitors`
                  SET `users_id` = '$uid'
               WHERE `guid` = '$guid'";
         return $DB->query($q);
      }
      return false;
   }

   function usersRelationshipsUpdate($guid, $name, $vid) {
      global $DB;
      $t_visitors = 'glpi_plugin_rocketchat_users_visitors';
      $q = "SELECT *
            FROM `$t_visitors`
            WHERE `visitors_name` = '$name'";
      if (($result = $DB->query($q)) && ($DB->numrows($result) == 1)) {
         $q = "UPDATE `$t_visitors`
                  SET `visitors_id` = '$vid',
                      `guid` = '$guid'
               WHERE `visitors_name` = '$name'";
         return $DB->query($q);
      } else {
         $q = "INSERT INTO `$t_visitors`
                  (`visitors_id`, `visitors_name` , `guid`)
               VALUES ('$vid', '$name', '$guid')";
         return $DB->query($q);
      }
   }

   function channelsRelationshipsAdd($id, $item_id, $item_type) {
      global $DB;
      $t_chats = 'glpi_plugin_rocketchat_chats_items';
      $q = "INSERT INTO `$t_chats`
               (`channel_id`, `items_id`, `itemtype`)
            VALUES ('$id', '$item_id', '$item_type')";
      return $DB->query($q) or false;
   }

   function textFromTemplate($template, $tokens) {
      if (preg_match_all('/(\{([a-z0-9]+)\})/si', $template, $matches)) {
         $search = reset($matches);
         $indexes = end($matches);
         $replace = [];
         foreach ($indexes as $token_name) {
            $token_name = mb_convert_case($token_name, MB_CASE_LOWER, 'UTF-8');
            if (is_array($tokens[$token_name]) && isset($tokens[$token_name][0])) {
               $replace []= $tokens[$token_name][0];
            } else {
               $replace []= $tokens[$token_name];
            }
         }
         return str_replace($search, $replace, $template);
      }
      return $template;
   }

   function textClean($text) {
      $text = Html::clean($text);
      $search = [ '\"' ,'\\\\', '&gt;', '&lt;', '\r\n', "\'"];
      $replace = [ '"', '\\', '>', '<', "\n", "'"];
      return str_replace($search, $replace, $text);
   }

   function guidToStr($guid) {
      // Magic
      $list = get_html_translation_table(HTML_ENTITIES);
      $values = array_keys($list);
      $search = array_values($list);
      $guid = str_replace($search, $values, $guid);
      // End magic
      return bin2hex($guid);
   }

}
