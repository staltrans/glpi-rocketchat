<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatHook {

   const COLOR_PRIMARY    = '#1b2f62';
   const COLOR_SECONDARY  = '#3a5693';
   const COLOR_TASK       = '#d99400';
   const COLOR_DOCUMENT   = '#00a500';
   const COLOR_COMMENT    = '#cf8b9e';
   const COLOR_SOLUTION   = '#93c6db';
   const COLOR_VALIDATION = '#c4c600';
   const COLOR_HIGHLIGHT  = '#ff0000';
   const COLOR_ACCEPTED   = '#4bd37b';
   const COLOR_REFUSED    = '#ff5a79';

   static function log($event, $level = 4) {
      Event::log(-1, 'plugin', $level, 'rocketchat', $event);
   }

   static function usersRelationships($data) {
      if (isset($data['_ldap_result'][0]['objectguid'][0])) {
         $user = new User();
         $user->getFromDBbyName($data['name']);
         $cli = new PluginRocketchatClient();
         $guid = $cli->guidToStr($data['_ldap_result'][0]['objectguid'][0]);
         $cli->usersRelationshipsUpdateUID($guid, $user->fields['id']);
      }
      return $data;
   }

   static function getActorsID(CommonITILObject $item, $types = []) {
      $useractors = new $item->userlinkclass();
      $all_actors = [];
      $actors_by_role = [];
      foreach ($useractors->getActors($item->fields['id']) as $role) {
         foreach ($role as $actor) {
            $all_actors[] = $actor['users_id'];
            foreach ($types as $t) {
               if ($actor['type'] == $t) {
                  $actors_by_role[$t][] = $actor['users_id'];
               }
            }
         }
      }
      $actors_by_role['all'] = $all_actors;
      return $actors_by_role;
   }

   static function getActorsNames(CommonITILObject $item, $types = []) {
      $useractors = new $item->userlinkclass();
      $all_actors = [];
      $actors_by_role = [];
      foreach ($useractors->getActors($item->fields['id']) as $role) {
         foreach ($role as $actor) {
            $all_actors[$actor['users_id']] = self::userName($actor['users_id']);
            foreach ($types as $t) {
               if ($actor['type'] == $t) {
                  $actors_by_role[$t][$actor['users_id']] = self::userName($actor['users_id']);
               }
            }
         }
      }
      $actors_by_role['all'] = $all_actors;
      return $actors_by_role;
   }

   static function groupName(CommonITILObject $item) {
      // Временное решение, переделать!!!
      $tmp_law_ids = [69, 70, 71, 72, 73, 87, 92];
      $tmp_hr_ids = [74];
      $tmp_aho_ids = [75];
      $tmp_szil_ids = [76];
      $tmp_mk_ids = [77];
      $tmp_id_ids = [78];
      $tmp_fd_ids = [79];
      $tmp_buh_ids = [80];
      $tmp_uu_ids = [81, 89, 90, 91];
      $tmp_tr_ids = [82];
      $tmp_drp_ids = [83];
      $tmp_dsp_ids = [84];
      $tmp_drazvp_ids = [85];
      $tmp_irk_ids = [88];

      $item_type = $item->getType();
      switch ($item_type) {
         case Ticket::getType():
            $group_name = 't';
            break;
         case Change::getType():
            $group_name = 'ch';
            break;
         case Problem::getType():
            $group_name = 'pr';
            break;
         default:
            $group_name = strtolower($item_type);
      }
      if (!empty($item->fields['itilcategories_id'])) {
         if (in_array($item->fields['itilcategories_id'], $tmp_law_ids)) {
            $group_name = "ur_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_hr_ids)) {
            $group_name = "hr_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_aho_ids)) {
            $group_name = "aho_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_szil_ids)) {
            $group_name = "szil_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_mk_ids)) {
            $group_name = "mk_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_id_ids)) {
            $group_name = "id_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_fd_ids)) {
            $group_name = "fd_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_buh_ids)) {
            $group_name = "buh_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_uu_ids)) {
            $group_name = "uu_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_tr_ids)) {
            $group_name = "tr_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_drp_ids)) {
            $group_name = "drp_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_dsp_ids)) {
            $group_name = "dsp_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_drazvp_ids)) {
            $group_name = "drazvp_{$group_name}";
         } else if (in_array($item->fields['itilcategories_id'], $tmp_irk_ids)) {
            $group_name = "irk_{$group_name}";
         } else {
            $group_name = "it_{$group_name}";
         }
      }
      return "{$group_name}{$item->fields['id']}_{$item->getName()}";
   }

   static function groupTopic(CommonITILObject $item, $use_markdown = true) {
      global $CFG_GLPI;
      $item_type = $item->getType();
      $url = ($use_markdown) ? '*' : '';
      $url .= '[';
      if ($item_type == Ticket::getType()) {
         $url .= $item->getTicketTypeName($item->fields['type']);
      } else {
         $url .= __($item_type);
      }
      $url .= " #{$item->fields['id']}]";
      $url .= ($use_markdown) ? "({$CFG_GLPI['url_base']}{$item->getLinkURL()})*" : '';
      return "$url {$item->getName()}";
   }

   static function itemAddITILObject(CommonITILObject $item) {

      global $CFG_GLPI;
      $cli = new PluginRocketchatClient();
      $item_id = $item->fields['id'];
      $item_type = $item->getType();
      $members = '';
      $user = new User();
      $uids = self::getActorsID($item);
      $uids = array_unique($uids['all']);
      $group_id = $cli->groupsCreate(self::groupName($item), $uids);
      if (!empty($group_id)) {
         if ($cli->channelsRelationshipsAdd($group_id, $item_id, $item_type)) {
            $attachments = [];
            $fields = [];
            $fields[] = [
               'short' => false,
               'title' => __('Description'),
               'value' => $cli->textClean($item->getField('content'))
            ];
            if (!empty($item->fields['itilcategories_id'])) {
               $itil_cat = new ITILCategory();
               $itil_cat->getFromDB($item->fields['itilcategories_id']);
               $fields[] = [
                  'short' => false,
                  'title' => __('Category'),
                  'value' => $itil_cat->getName()
               ];
            }
            $fields[] = [
               'short' => false,
               'title' => __('Requester'),
               'value' => self::userName($item->fields['users_id_recipient'])
            ];
            $attachments[] = [
               'color' => self::COLOR_PRIMARY,
               'collapsed' => false,
               'title' => self::groupTopic($item, false),
               'fields' => $fields
            ];
            $fields = [];
            foreach ($uids as $uid) {
               $user_info = '';
               $user->getFromDB($uid);
               $phone = __('не указан', 'rocketchat');
               if (!empty($user->fields['phone'])) {
                  $phone = $user->fields['phone'];
               }
               $user_info .= __('Phone') . ": $phone\n";
               $user_info .= __('Email') . ': ';
               $mail = $user->getDefaultEmail();
               if (empty($mail)) {
                  $user_info .= __('не указан', 'rocketchat');
               } else {
                  $user_info .= $mail;
               }
               $fields[] = [
                  'short' => false,
                  'title' => $user->getName(),
                  'value' => $user_info
               ];
            }
            $count = count($fields);
            if ($count > 0) {
               $attachments[] = [
                  'color' => self::COLOR_SECONDARY,
                  'collapsed' => true,
                  'title' => __('Участники', 'rocketchat'),
                  'fields' => $fields
               ];
            }
            $cli->groupsSetTopic($group_id, self::groupTopic($item));
            $cli->chatSendMessage($group_id, [
               //'text' => $topic,
               'attachments' => $attachments
            ]);

            $doc_item = new Document_Item();
            $doc = new Document();
            $condition = "`itemtype` = '$item_type' AND `items_id` = '$item_id'";
            //$attachments = [];
            $attachments = '';
            foreach ($doc_item->find($condition) as $di) {
               $doc->getFromDB($di['documents_id']);
               $url = "{$CFG_GLPI['url_base']}{$CFG_GLPI['root_doc']}/front/document.send.php?docid={$doc->fields['id']}";
               if ($item_type == 'Ticket') {
                  $url .= "&tickets_id=$item_id";
               }
               //$attachments .= "[{$doc->fields['filename']}]($url)\n";
               // https://h.staltrans.ru/front/problem.form.php?id=206
               $attachments .= "*{$doc->fields['filename']}* ссылка $url\n";
               if (!empty($doc->fields['comment'])) {
                  $attachments .= $cli->textClean($doc->fields['comment']) . "\n";
               }
               // https://h.staltrans.ru/front/ticket.form.php?id=9025
               /*$attachments[] = [
                  'color' => self::COLOR_DOCUMENT,
                  'title' => $doc->fields['filename'],
                  'title_link' => $url,
                  'title_link_download' => true,
                  'text' => $cli->textClean($doc->fields['comment'])
               ];*/
            }
            if (!empty($attachments)) {
               $cli->chatSendMessage($group_id, [
                  'text' => sprintf(__('%s добавляет документ'), self::userName($di['users_id'])) . "\n$attachments",
                  //'attachments' => $attachments
               ]);
            }

         } else {
            self::log(sprintf(__('Не удалось создать привязку для канала %s', 'rocketchat'), $group_id));
         }
      }
   }

   static function itemAddITILSolution(ITILSolution $item) {
      global $CFG_GLPI;
      PluginRocketchatDebug::variable('itilsolution', $item);
      if (class_exists($item->fields['itemtype'])) {
         $obj = new $item->fields['itemtype'];
         $cli = new PluginRocketchatClient();
         $obj->getFromDB($item->fields['items_id']);
         $channel_id = $cli->getChannelId($item->fields['items_id'], $item->fields['itemtype']);
         if (!empty($channel_id)) {
            $attachments = [[
               'color' => self::COLOR_SOLUTION,
               'title' => __('Solution'),
               'text' => $cli->textClean($item->getField('content'))
            ]];
            $useractors = self::getActorsNames($obj, [CommonITILActor::REQUESTER]);
            $text = join(', ', $useractors[CommonITILActor::REQUESTER]) . ",\n";
            if ($item->fields['itemtype'] == 'Ticket') {
               // Не использую markdown из-за бага https://github.com/RocketChat/Rocket.Chat/issues/6586
               $text .= __('утвердите или отклоните решение, перейдя по ссылке', 'rocketchat') . "\n";
               $text .= "{$CFG_GLPI['url_base']}{$obj->getLinkURL()}&forcetab=Ticket$1\n";
               $text .= __('Или оцените работу специалистов, отправив число от *2* до *5* в чат.', 'rocketchat') . "\n";
               $text .= __('Для отклонения решения отправьте в чат ноль *0* и напишите причину через пробел(ы) после нуля.', 'rocketchat') . "\n";
               $attachments[] = [
                  'color' => self::COLOR_HIGHLIGHT,
                  'text' => $text
               ];
            }
            $cli->chatSendMessage($channel_id, [
               'attachments' => $attachments,
            ]);
         }
      }
   }

   static function itemAddITILTask(CommonITILTask $item) {
      if ((isset($item->fields['is_private']) && $item->fields['is_private'] == 0)
         || !isset($item->fields['is_private'])) {
         if (isset($item->fields['problems_id'])) {
            $item_type = 'Problem';
            $item_id = $item->fields['problems_id'];
         }
         if (isset($item->fields['tickets_id'])) {
            $item_type = 'Ticket';
            $item_id = $item->fields['tickets_id'];
         }
         if (isset($item->fields['changes_id'])) {
            $item_type = 'Change';
            $item_id = $item->fields['changes_id'];
         }
         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($item_id, $item_type);
         if (!empty($channel_id)) {
            $fields = [];
            if (!empty($item->fields['actiontime'])) {
               if (!empty($item->fields['begin'])) {
                  $fields[] = [
                     'short' => true,
                     'title' => __('Начало', 'rocketchat') . ':',
                     'value' => Html::convDateTime($item->fields['begin'])
                  ];
               }
               if (!empty($item->fields['end'])) {
                  $fields[] = [
                     'short' => true,
                     'title' => __('Окончание', 'rocketchat') . ':',
                     'value' => Html::convDateTime($item->fields['end'])
                  ];
               }
               $fields[] = [
                  'short' => false,
                  'title' => __('Duration') . ':',
                  'value' => Html::timestampToString($item->fields['actiontime'], 0)
               ];
            }
            if (!empty($item->fields['users_id_tech'])) {
               $text = sprintf(__('%s добавляет задачу', 'rocketchat'), self::userName($item->fields['users_id_tech']));
            } else {
               $text = __('Добавлена задача', 'rocketchat');
            }
            $cli->chatSendMessage($channel_id, [
               'text' => $text,
               'attachments' => [[
                  'color' => self::COLOR_TASK,
                  'title' => __('Description'),
                  'text' => $cli->textClean($item->getField('content')),
                  'fields' => $fields
               ]]
            ]);
         }
      }
   }

   static function itemAddTicketFollowup(TicketFollowup $item) {
      if ($item->fields['is_private'] == 0) {
         $cfg = new PluginRocketchatConfig();
         $opt = $cfg->get($cfg::VAR_SETTINGS);
         if (!isset($item->fields['requesttypes_id']) || $item->fields['requesttypes_id'] != $opt->requesttypes_id) {
            $cli = new PluginRocketchatClient();
            $channel_id = $cli->getChannelId($item->fields['tickets_id'], 'Ticket');
            if (!empty($channel_id)) {
               $text = sprintf(__('%s adds a followup'), self::userName($item->fields['users_id']));
               $cli->chatSendMessage($channel_id, [
                  'text' => $text,
                  'attachments' => [[
                     'color' => self::COLOR_COMMENT,
                     'title' => ' ',
                     'text' => $cli->textClean($item->getField('content')),
                  ]]
               ]);
            }
         }
      }
   }

   static function itemAddDocumentItem(Document_Item $item) {
      $cli = new PluginRocketchatClient();
      $item_type = $item->fields['itemtype'];
      $item_id = $item->fields['items_id'];
      $channel_id = $cli->getChannelId($item_id, $item_type);
      if (!empty($channel_id)) {
         global $CFG_GLPI;
         $url = $CFG_GLPI['url_base'] . $CFG_GLPI['root_doc'];
         if (($item_type == 'Change' || $item_type == 'Problem') && self::isActivPlugin('documentalter')) {
            $doc = new PluginDocumentalterDocument();
            $url .= '/plugins/documentalter/front/document.send.php?docid=';
         } else {
            $doc = new Document();
            $url .= '/front/document.send.php?docid=';
         }
         $doc->getFromDB($item->fields['documents_id']);
         $text = sprintf(__('%s добавляет документ'), self::userName($item->fields['users_id']));
         $url .= $doc->fields['id'];
         if ($item_type == 'Ticket') {
            $url .= "&tickets_id=$item_id";
         }
         // https://github.com/RocketChat/Rocket.Chat/issues/6719
         /*if ($item_type == 'Ticket') {
            $url .= "&tickets_id=$item_id";
         }*/
         // https://h.staltrans.ru/front/problem.form.php?id=206
         //$text .= "\n[{$doc->fields['filename']}]($url)";
         $text .= "\n*{$doc->fields['filename']}* ссылка $url";
         $cli->chatSendMessage($channel_id, [
            'text' => $text,
            // https://h.staltrans.ru/front/ticket.form.php?id=9025
            /*'attachments' => [[
               'color' => self::COLOR_DOCUMENT,
               'title' => $doc->fields['filename'],
               'title_link' => $url,
               'title_link_download' => true,
               'text'  => $cli->textClean($doc->fields['comment'])
            ]]*/
         ]);
      }
   }

   static function itemAddITILActor(CommonITILActor $item) {
      if (isset($item->fields['problems_id'])) {
         $item_type = 'Problem';
         $item_id = $item->fields['problems_id'];
      }
      if (isset($item->fields['tickets_id'])) {
         $item_type = 'Ticket';
         $item_id = $item->fields['tickets_id'];
      }
      if (isset($item->fields['changes_id'])) {
         $item_type = 'Change';
         $item_id = $item->fields['changes_id'];
      }
      $cli = new PluginRocketchatClient();
      $channel_id = $cli->getChannelId($item_id, $item_type);
      if (!empty($channel_id)) {
         $vids = $cli->getVisitorsIds([$item->fields['users_id']]);
         if (isset($vids[0])) {
            $cli->groupsInvite($channel_id, $vids[0]);
         }
      }
   }

   static function itemAddNotepad(Notepad $item) {
      if (!empty($item->fields['users_id']) && class_exists('PluginRocketchatClient')) {
         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($item->fields['items_id'], $item->fields['itemtype']);
         if (!empty($channel_id)) {
            $text = sprintf(__('%s adds a note', 'rocketchat'), self::userName($item->fields['users_id']));
            $cli->chatSendMessage($channel_id, [
               'text' => $text,
               'attachments' => [[
                  'color' => self::COLOR_COMMENT,
                  'title' => ' ',
                  'text' => $cli->textClean($item->getField('content')),
               ]]
            ]);
         }
      }
   }

   static function itemAddCommonITILValidation(CommonITILValidation $item) {

      if (isset($item->fields['users_id_validate'])) {

         $from_uid = $item->fields['users_id'];
         $to_uid = $item->fields['users_id_validate'];

         if (isset($item->fields['tickets_id'])) {
            $obj_type = 'Ticket';
            $obj_id = $item->fields['tickets_id'];
         }

         if (isset($item->fields['changes_id'])) {
            $obj_type = 'Change';
            $obj_id = $item->fields['changes_id'];
         }

         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($obj_id, $obj_type);
         if (!empty($channel_id)) {
            foreach ($cli->getVisitorsIds([$to_uid]) as $vid) {
               $cli->groupsInvite($channel_id, $vid);
            }
            $attachments = [];
            if (!empty($item->fields['comment_submission'])) {
               $attachments[] = [
                  'color' => self::COLOR_VALIDATION,
                  'title' => __('Request comments'),
                  'text'  => $cli->textClean($item->fields['comment_submission'])
               ];
            }
            $attachments[] = [
               'color' => self::COLOR_HIGHLIGHT,
               'text'  => __("Отправьте в чат *1* для утверждения запроса, или *0* для отклонения.", 'rocketchat'),
            ];
            $text = sprintf(__("%s, вам отправлен запрос на согласование", 'rocketchat'), self::nicknameOrName($to_uid));
            $cli->chatSendMessage($channel_id, [
               'text' => $text,
               'attachments' => $attachments
            ]);
         }

      }

   }

   static function itemUpdateITILObject(CommonITILObject $item) {

      global $CFG_GLPI;
      $item_id = $item->fields['id'];
      $item_type = $item->getType();
      $cli = new PluginRocketchatClient();
      $channel_id = $cli->getChannelId($item_id, $item_type);
      if (empty($channel_id)) {
         self::itemAddITILObject($item);
         $channel_id = $cli->getChannelId($item_id, $item_type);
      }
      if (!empty($channel_id)) {
         $text = '';
         $fields = [];
         $attachments = [];
         foreach ($item->updates as $field) {
            switch ($field) {

               case 'name':

                  if ($cli->textClean($item->oldvalues[$field]) != $cli->textClean($item->fields[$field])) {
                     $cli->groupsRename($channel_id, self::groupName($item));
                     $cli->groupsSetTopic($channel_id, self::groupTopic($item));
                  }

                  break;

               case 'status':

                  $useractors = self::getActorsNames($item, [CommonITILActor::REQUESTER]);

                  if (in_array($item->fields[$field], $item::getClosedStatusArray())) {
                     if (($item_type == 'Change' || $item_type == 'Problem')
                     && self::isActivPlugin('itemrating')) {
                        $text = join(', ', $useractors[CommonITILActor::REQUESTER]) . ",\n";
                        $text .= __('для оценки работы специалистов перейдите по ссылке', 'rocketchat') . "\n";
                        $text .= "{$CFG_GLPI['url_base']}{$item->getLinkURL()}&forcetab=PluginItemratingRating$1\n";
                        $text .= __('Или отправьте сообщение в чат, содержащее число от *2* до *5*.', 'rocketchat') . "\n";
                        $text .= __('Через пробел(ы) после оценки вы можете написать комментарий.', 'rocketchat');
                        $attachments[] = [
                           'color' => self::COLOR_HIGHLIGHT,
                           'text' => $text
                        ];
                     }
                  } else if (in_array($item->oldvalues[$field], $item::getClosedStatusArray())) {
                     if ($cli->groupsUnarchive($channel_id)) {
                        foreach ($cli->getVisitorsIds(array_keys($useractors['all'])) as $vid) {
                           $cli->groupsInvite($channel_id, $vid);
                        }
                     }
                  }

                  $fields[] = [
                     'short' => false,
                     'title' => __('Status') . ':',
                     'value' => $item::getStatus($item->fields[$field])
                  ];
                  break;

               case 'content':

                  $content = $cli->textClean($item->fields[$field]);
                  if ($cli->textClean($item->oldvalues[$field]) != $content) {
                     $fields[] = [
                        'short' => false,
                        'title' => __('Content') . ':',
                        'value' => $content
                     ];
                  }
                  break;

               case 'global_validation':

                  /*$fields[] = [
                     'short' => false,
                     'title' => __('Validation') . ':',
                     'value' => CommonITILValidation::getStatus($item->fields[$field])
                  ];*/
                  break;

               case 'itilcategories_id':

                  $itil_cat = new ITILCategory();
                  $itil_cat->getFromDB($item->fields[$field]);
                  $fields[] = [
                     'short' => false,
                     'title' => __('Category') . ':',
                     'value' => "[{$itil_cat->getName()}]({$CFG_GLPI['url_base']}{$itil_cat->getLinkURL()})"
                  ];
                  $cli->groupsRename($channel_id, self::groupName($item));
                  break;

               case 'type':

                  $fields[] = [
                     'short' => false,
                     'title' => __('Type') . ':',
                     'value' => $item->getTicketTypeName($item->fields[$field])
                  ];
                  $cli->groupsSetTopic($channel_id, self::groupTopic($item));
                  break;

               case 'solvedate':

                  if ($item->fields[$field] == 'NULL') {
                     $attachments[] = [
                        'color' => self::COLOR_HIGHLIGHT,
                        'text' => "*" . __('Solution rejected') . "*"
                     ];
                  }
                  break;

            }
         }
         if (!empty($fields)) {
            $collapsed = true;
            if (count($fields) == 1) {
               if (mb_strlen($fields[0]['value']) < 100) {
                  $collapsed = false;
               }
            }
            $attachments = array_merge([[
               'collapsed' => $collapsed,
               'color' => self::COLOR_COMMENT,
               'title' => __('Обновлены поля', 'rocketchat'),
               'fields' => $fields
            ]], $attachments);
         }
         if (!empty($attachments)) {
            $text = '';
            $user = self::userName($item->fields['users_id_lastupdater']);
            switch ($item_type) {
               case 'Ticket':
                  $text = sprintf(__('%s обновляет заявку', 'rocketchat'), $user);
                  break;
               case 'Change':
                  $text = sprintf(__('%s обновляет изменение', 'rocketchat'), $user);
                  break;
               case 'Problem':
                  $text = sprintf(__('%s обновляет проблему', 'rocketchat'), $user);
                  break;
            }
            $cli->chatSendMessage($channel_id, [
               'text' => $text,
               'attachments' => $attachments
            ]);
         }
      }
   }

   static function itemUpdateITILTask(CommonITILTask $item) {
      if ((isset($item->fields['is_private']) && $item->fields['is_private'] == 0)
         || !isset($item->fields['is_private'])) {
         if (isset($item->fields['problems_id'])) {
            $item_type = 'Problem';
            $item_id = $item->fields['problems_id'];
         }
         if (isset($item->fields['tickets_id'])) {
            $item_type = 'Ticket';
            $item_id = $item->fields['tickets_id'];
         }
         if (isset($item->fields['changes_id'])) {
            $item_type = 'Change';
            $item_id = $item->fields['changes_id'];
         }
         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($item_id, $item_type);
         if (!empty($channel_id)) {
            $attach_fields = [];
            foreach ($item->updates as $field) {
               switch ($field) {
                  case 'taskcategories_id':
                     if (!empty($item->fields[$field])) {
                        $category = new TaskCategory();
                        $category->getFromDB($item->fields[$field]);
                        $attach_fields[] = [
                           'short' => false,
                           'title' => __('Category') . ':',
                           'value' => $category->getName()
                        ];
                     }
                     break;
                  case 'state':
                     $attach_fields[] = [
                        'short' => false,
                        'title' => _x('item', 'State') . ':',
                        'value' => Planning::getState($item->fields[$field])
                     ];
                     break;
                  case 'begin':
                     $begin = $item->fields[$field];
                     break;
                  case 'end':
                     $end = $item->fields[$field];
                     break;
                  case 'users_id_tech':
                     $attach_fields[] = [
                        'short' => false,
                        'title' => __('Doer') . ':',
                        'value' => self::userName($item->fields[$field])
                     ];
                     break;
                  case 'actiontime':
                     $duration = Html::timestampToString($item->fields[$field], 0);
                     break;
               }
            }
            if (!empty($duration)) {
                $attach_fields[] = [
                  'short' => false,
                  'title' => __('Duration') . ':',
                  'value' => $duration
                ];
            }
            if (!empty($begin)) {
                $attach_fields[] = [
                  'short' => true,
                  'title' => __('Начало', 'rocketchat') . ':',
                  'value' => Html::convDateTime($begin)
                ];
            }
            if (!empty($end)) {
                $attach_fields[] = [
                  'short' => true,
                  'title' => __('Окончание', 'rocketchat') . ':',
                  'value' => Html::convDateTime($end)
                ];
            }
            if (!empty($attach_fields)) {
               $cli->chatSendMessage($channel_id, [
                  'text' => sprintf(__('%s обновляет задачу', 'rocketchat'), self::userName($item->fields['users_id_editor'])),
                  'attachments' => [[
                     'color' => self::COLOR_TASK,
                     'title' => __('Description'),
                     'text' => $cli->textClean($item->fields['content']),
                     'fields' => $attach_fields
                  ]]
               ]);
            }
         }
      }
   }

   static function itemDeleteITILObject(CommonITILObject $item) {
      $cli = new PluginRocketchatClient();
      $channel_id = $cli->getChannelId($item->fields['id'], $item->getType());
      if (!empty($channel_id)) {
         $useractors = self::getActorsID($item);
         foreach ($cli->getVisitorsIds($useractors['all']) as $vid) {
            $cli->groupsKick($channel_id, $vid);
         }
         $cli->groupsArchive($channel_id);
      }
   }

   static function itemRestoreITILObject(CommonITILObject $item) {
      $cli = new PluginRocketchatClient();
      $channel_id = $cli->getChannelId($item->fields['id'], $item->getType());
      if (!empty($channel_id)) {
         if ($cli->groupsUnarchive($channel_id)) {
            $useractors = self::getActorsID($item);
            foreach ($cli->getVisitorsIds($useractors['all']) as $vid) {
               $cli->groupsInvite($channel_id, $vid);
            }
         }
      }
   }

   static function itemPurgeITILActor(CommonITILActor $item) {
      if (isset($item->fields['problems_id'])) {
         $item_type = 'Problem';
         $item_id = $item->fields['problems_id'];
      }
      if (isset($item->fields['tickets_id'])) {
         $item_type = 'Ticket';
         $item_id = $item->fields['tickets_id'];
      }
      if (isset($item->fields['changes_id'])) {
         $item_type = 'Change';
         $item_id = $item->fields['changes_id'];
      }
      $cli = new PluginRocketchatClient();
      $channel_id = $cli->getChannelId($item_id, $item_type);
      if (!empty($channel_id)) {
         $vids = $cli->getVisitorsIds([$item->fields['users_id']]);
         if (isset($vids[0])) {
            $cli->groupsKick($channel_id, $vids[0]);
         }
      }
   }

   static function itemUpdateTicketSatisfaction(TicketSatisfaction $item) {
      if ($item->fields['satisfaction']) {
         $tid = $item->fields['tickets_id'];
         $ticket = new Ticket();
         $ticket->getFromDB($tid);
         $ticket_type = $ticket->getType();
         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($tid, $ticket_type);
         if (!empty($channel_id)) {
            $text = 'Спасибо за оценку нашей работы';
            $cli->chatSendMessage($channel_id, $text);
            $useractors = self::getActorsID($ticket, [CommonITILActor::REQUESTER]);
            foreach ($cli->getVisitorsIds($useractors[CommonITILActor::REQUESTER]) as $vid) {
               $cli->groupsKick($channel_id, $vid);
            }
            //$cli->groupsArchive($channel_id);
            $cli->groupsSetReadOnly($channel_id);
         }
      }
   }

   static function itemUpdateRating(PluginItemratingRating $item) {
      if (!empty($item->fields['rating'])) {
         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($item->fields['items_id'], $item->fields['itemtype']);
         if (!empty($channel_id)) {
            $text = sprintf(__('%s, спасибо за оценку нашей работы.'), self::userName($item->fields['users_id']));
            $cli->chatSendMessage($channel_id, $text);
            $uids = [ $item->fields['users_id'] ];
            foreach ($cli->getVisitorsIds($uids) as $vid) {
               $cli->groupsKick($channel_id, $vid);
            }
            //$cli->groupsArchive($channel_id);
         }
      }
   }

   static function itemUpdateCommonITILValidation(CommonITILValidation $item) {

      if (in_array('status', $item->updates)) {

         $from_uid = $item->fields['users_id'];
         $to_uid = $item->fields['users_id_validate'];

         if (isset($item->fields['tickets_id'])) {
            $obj_type = 'Ticket';
            $obj_id = $item->fields['tickets_id'];
         }

         if (isset($item->fields['changes_id'])) {
            $obj_type = 'Change';
            $obj_id = $item->fields['changes_id'];
         }

         $cli = new PluginRocketchatClient();
         $channel_id = $cli->getChannelId($obj_id, $obj_type);
         if (!empty($channel_id)) {
            $text = '';
            $from = self::userName($from_uid);
            $to = self::userName($to_uid);
            $comment = $cli->textClean($item->fields['comment_validation']);
            switch ($item->fields['status']) {
               case CommonITILValidation::ACCEPTED:
                  /*foreach ($cli->getVisitorsIds([$to_uid]) as $vid) {
                     $cli->groupsKick($channel_id, $vid);
                  }*/
                  $color = self::COLOR_ACCEPTED;
                  $text .= sprintf(__(":white_check_mark: %s утверждает запрос", 'rocketchat'), $to);
                  break;
               case CommonITILValidation::REFUSED:
                  $color = self::COLOR_REFUSED;
                  $text .= sprintf(__(":negative_squared_cross_mark: %s отклоняет запрос", 'rocketchat'), $to);
                  break;
               default:
                  $status = CommonITILValidation::getStatus($item->fields['status']);
                  $color = self::COLOR_COMMENT;
                  $text .= sprintf(__("%s меняет статус согласования на '%s'", 'rocketchat'), $to, $status);
                  break;
            }
            $msg = [
               'text' => $text,
            ];
            if (!empty($comment)) {
               $msg['attachments'] = [[
                  'color' => $color,
                  'title' => __('Approval comments'),
                  'text' => $comment
               ]];
            }
            $cli->chatSendMessage($channel_id, $msg);
         }

      }

   }

   static function isActivPlugin($plugin_name) {
      $plug = new Plugin();
      return $plug->isActivated($plugin_name);
   }

   static function author($users_id = 0) {
      if (!empty($users_id)) {
         $user = new User();
         $user->getFromDB($users_id);
         return [
            'author_link' => $user->getLinkUrl(),
            'author_name' => $user->getName(),
         ];
      }
      return [];
   }

   static function userName($users_id) {
      global $CFG_GLPI;
      $user = new User();
      if ($user->getFromDB($users_id)) {
         return "[{$user->getName()}]({$CFG_GLPI['url_base']}{$user->getLinkURL()})";
      }
      return '';
   }

   static function nicknameOrName($users_id) {
      $cli = new PluginRocketchatClient();
      $nicks = $cli->getVisitorsNames([$users_id]);
      if (isset($nicks[0])) {
         return '@' . $nicks[0];
      } else {
         return self::userName($users_id);
      }
      return '';
   }

   static function testHook($item) {
      PluginRocketchatDebug::variable('item', $item);
   }

}
