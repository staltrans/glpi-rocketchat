<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatCron {

   static function getTypeName($nb = 0) {
      return _n('Задача Rocket.Chat', 'Задачи Rocket.Chat', $nb);
   }

   static function log($event, $level = 4) {
      Event::log(-1, 'pluginrocketchat', $level, 'rocketchat', $event);
   }

   static function taskLog($msg, $task = null) {
      if (is_object($task)) {
         $task->log($msg);
      } else {
         self::log($msg);
      }
   }

   static function __($str) {
      return __($str, 'rocketchat');
   }

   static function cronInfo($name) {
      switch ($name) {
         case 'UsersFetch':
            return ['description' => self::__('Синхронизация пользователей из rocket.chat')];
         case 'CheckLogin':
            return ['description' => self::__('Проверка статуса аккаунта бота')];
         case 'ChannelKickAll':
            return ['description' => self::__('Удаление участников из каналов закрытых заявок')];
      }
      return [];
   }

   static function cronUsersFetch($task = null) {
      $cli = new PluginRocketchatClient();
      $users = $cli->usersList();
      if (!empty($users)) {
         $i = 0;
         foreach ($users as $user) {
            if (isset($user->services->ldap->id)) {
               $guid = $user->services->ldap->id;
               if ($cli->usersRelationshipsUpdate($guid, $user->username, $user->_id)) {
                  $i++;
               }
            }
         }
         self::taskLog(sprintf(self::__('Синхронизировано %s записей пользователей'), $i), $task);
         return 1;
      }
      self::taskLog(self::__('Ошибка синхронизации'), $task);
      return 0;
   }

   static function cronCheckLogin($task = null) {
      $cli = new PluginRocketchatClient();
      $about = $cli->checkMe();
      if (!empty($about)) {
         self::taskLog(sprintf(self::__('Учётная запись бота %s авторизована'), $about->name), $task);
         return 1;
      } else {
         $user_id = $cli->getUserId();
         $auth_token = $cli->getAuthToken();
         $bot_account = $cli->getBotAccount();
         self::taskLog(sprintf(self::__('Ошибка авторизации аккаунта бота %s'), $about->name), $task);
         return 0;
      }
   }

   static function cronChannelKickAll($task = null) {
      global $DB;
      $cli = new PluginRocketchatClient();
      $now = new DateTime();
      $date_interval = new DateInterval('P1W');
      $date_interval2 = new DateInterval('P2D');
      $now->sub($date_interval2);
      $end = $now->format('Y-m-d');
      $now->sub($date_interval);
      $start = $now->format('Y-m-d');
      $obj_ratings = [ new TicketSatisfaction() ];
      if (PluginRocketchatHook::isActivPlugin('itemrating')) {
         $obj_ratings[] = new PluginItemratingRating();
      }
      foreach ($obj_ratings as $rating) {
         switch ($rating->getType()) {
            case 'TicketSatisfaction':
               $query = "SELECT 'Ticket' AS 'itemtype', `tickets_id` AS `items_id`
                  FROM `{$rating->getTable()}`
                  WHERE `satisfaction` IS NOT NULL AND `date_answered` < '$end' AND `date_answered` > '$start'";
               break;
            case 'PluginItemratingRating':
               $query = "SELECT `itemtype`, `items_id`
                  FROM `{$rating->getTable()}`
                  WHERE `rating` IS NOT NULL AND `date_mod` < '$end' AND `date_mod` > '$start'
                  GROUP BY `itemtype`, `items_id`";
               break;
         }
         if (isset($query)) {
            foreach ($DB->request($query) as $data) {
               if (class_exists($data['itemtype'])) {
                  $obj = new $data['itemtype'];
                  if ($obj->getFromDB($data['items_id'])) {
                     if(in_array($obj->fields['status'], $obj::getClosedStatusArray())) {
                        $useractors = new $obj->userlinkclass();
                        $channel_id = $cli->getChannelId($data['items_id'], $data['itemtype']);
                        if (!empty($channel_id)) {
                           foreach ($useractors->getActors($data['items_id']) as $role) {
                              foreach ($role as $actor) {
                                 foreach ($cli->getVisitorsIds([$actor['users_id']]) as $vid) {
                                    PluginRocketchatDebug::variable('cronChannelKickAll', "Kick $vid from $channel_id");
                                    $cli->groupsUnarchive($channel_id);
                                    $cli->groupsKick($channel_id, $vid);
                                    $cli->groupsArchive($channel_id);
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

}
