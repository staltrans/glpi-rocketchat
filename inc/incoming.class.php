<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatIncoming {

   private $data;
   private $opt;

   function __construct() {
      $cfg = new PluginRocketchatConfig();
      $this->opt = $cfg->get($cfg::VAR_SETTINGS);
   }

   private function decode($data) {
      return (array) json_decode($data);
   }

   private function eq($val1 = null, $val2 = null) {
      if (!empty($val1) && !empty($val2) && $val1 == $val2) {
            return true;
      }
      return false;
   }

   private function getUserId($visitor_id) {
      global $DB;
      $t_visitors = 'glpi_plugin_rocketchat_users_visitors';
      $q = "SELECT `users_id`
               FROM `$t_visitors`
            WHERE `visitors_id` = '$visitor_id'";
      if (($result = $DB->query($q)) && ($DB->numrows($result))) {
         if (!empty($data = $DB->fetch_assoc($result))) {
            return $data['users_id'];
         }
      }
      return false;
   }

   private function getItemId($channel_id) {
      global $DB;
      $t_chats = 'glpi_plugin_rocketchat_chats_items';
      $q = "SELECT `items_id`, `itemtype`
               FROM `$t_chats`
            WHERE `channel_id` = '$channel_id'";
      if (($result = $DB->query($q)) && ($DB->numrows($result) == 1)) {
         if (!empty($data = $DB->fetch_assoc($result))) {
            return $data;
         }
      }
      return false;
   }

   function incomingURL() {
      global $CFG_GLPI;
      $path_info = '';
      if (isset($_SERVER['PATH_INFO'])) {
         $path_info = $_SERVER['PATH_INFO'];
      }
      return $CFG_GLPI['url_base'] . Toolbox::getItemTypeFormURL(get_called_class(), true) . $path_info;
   }

   function process($data) {

      PluginRocketchatDebug::variable('data', $data);
      $path_info = '';
      if (isset($_SERVER['PATH_INFO'])) {
         $path_info = $_SERVER['PATH_INFO'];
      }
      $this->data = $this->decode($data);
      if (isset($this->data['user_id']) && $this->data['user_id'] != $this->getBotID()) {
         $webhook = new PluginRocketchatWebhook();
         $condition = "`url`='" . $this->incomingURL() . "' AND `token`='" . $this->data['token'] . "'";
         $result = $webhook->find($condition);
         $cnt = count($result);
         if ($cnt == 1) {
            $result = reset($result);
            $uid = $this->getUserId($this->data['user_id']);
            $item = $this->getItemId($this->data['channel_id']);
            switch ($result['event']) {
               case PluginRocketchatWebhook::SEND_MESSAGE:
                  if (!in_array($this->data['text'], ['Uploaded an image', 'Uploaded a file'])) {
                     switch ($item['itemtype']) {
                        case 'Ticket':

                           $ticket_valid = new TicketValidation();
                           $ok = $ticket_valid->getFromDBByRequest([
                              'WHERE' => [
                                 'tickets_id' => $item['items_id'],
                                 'users_id_validate' => $uid,
                                 'status' => [
                                    CommonITILValidation::WAITING,
                                    CommonITILValidation::NONE
                                 ]
                              ],
                              'LIMIT' => 1
                           ]);
                           if ($ok) {
                              $approve = $this->explodeMesage($this->data['text']);
                              if (!empty($approve)) {
                                 if ($approve['control'] == '1' || $approve['control'] == '0') {
                                    $this->loadSession($uid);
                                    $this->loadHooks();
                                    $this->updateValidation($uid, $item['items_id'], $item['itemtype'], $approve['comment'], $approve['control']);
                                    break;
                                 }
                              }
                           }

                           $ticket = new Ticket();
                           $ticket->getFromDB($item['items_id']);
                           if (in_array($ticket->fields['status'], $ticket->getSolvedStatusArray())) {
                              $approve = $this->explodeMesage($this->data['text']);
                              if (!empty($approve)) {
                                 if ($approve['control'] == '0') {
                                    $this->loadSession($uid);
                                    $this->loadHooks();
                                    if (empty($approve['comment'])) {
                                       $approve['comment'] = '&nbsp;';
                                    }
                                    $this->addTicketFollowup($uid, $item['items_id'], $approve['comment'], $approve['control']);
                                    break;
                                 } else if ($approve['control'] > 1 && $approve['control'] <= 5) {
                                    $this->loadSession($uid);
                                    $this->loadHooks();
                                    $this->addTicketFollowup($uid, $item['items_id'], $approve['comment'], 1);
                                    $ts = new TicketSatisfaction();
                                    if ($ts->getFromDBByRequest(['WHERE' => ['tickets_id' => $item['items_id']]])) {
                                       $ts->fields['satisfaction'] = $approve['control'];
                                       $ts->fields['comment'] = $approve['comment'];
                                       $ts->update($ts->fields);
                                       break;
                                    }
                                 }
                              }
                           } else if (in_array($ticket->fields['status'], $ticket->getClosedStatusArray())) {
                              $ts = new TicketSatisfaction();
                              if ($ts->getFromDBByRequest(['WHERE' => ['tickets_id' => $item['items_id']]])) {
                                 $satisfaction = $this->explodeMesage($this->data['text']);
                                 if (!empty($satisfaction)) {
                                    if ($star = intval($satisfaction['control'])) {
                                       if ($star >= 1 && $star <= 5) {
                                          $ts->fields['satisfaction'] = $star;
                                          $ts->fields['comment'] = $satisfaction['comment'];
                                          $ts->update($ts->fields);
                                          break;
                                       }
                                    }
                                 }
                              }
                           }
                           $this->addTicketFollowup($uid, $item['items_id'], $this->data['text']);
                           break;
                        case 'Problem':
                        case 'Change':

                           $this->loadSession($uid);
                           $this->addNotepad($uid, $item['itemtype'], $item['items_id'], $this->data['text']);
                           $this->loadHooks();

                           if ($item['itemtype'] == 'Change') {
                              $change_valid = new ChangeValidation();
                              $ok = $change_valid->getFromDBByRequest([
                                 'WHERE' => [
                                    'changes_id' => $item['items_id'],
                                    'users_id_validate' => $uid
                                 ],
                                 'LIMIT' => 1
                              ]);
                              if ($ok) {
                                 $approve = $this->explodeMesage($this->data['text']);
                                 if (!empty($approve)) {
                                    if ($approve['control'] == '1' || $approve['control'] == '0') {
                                       $this->updateValidation($uid, $item['items_id'], $item['itemtype'], $approve['comment'], $approve['control']);
                                       break;
                                    }
                                 }
                              }
                           }

                           if (PluginRocketchatHook::isActivPlugin('itemrating') && class_exists($item['itemtype'])) {
                              $itilobj = new $item['itemtype'];
                              $itilobj->getFromDB($item['items_id']);
                              if (in_array($itilobj->fields['status'], $itilobj->getClosedStatusArray())) {
                                 $rating = new PluginItemratingRating();
                                 if ($rating->getFromDBByItemUser($item['itemtype'], $item['items_id'], $uid)) {
                                    $satisfaction = $this->explodeMesage($this->data['text']);
                                    if (!empty($satisfaction)) {
                                       if ($star = intval($satisfaction['control'])) {
                                          if ($star > 1 && $star <= 5) {
                                             $rating->fields['rating'] = $star;
                                             $rating->fields['comment'] = $satisfaction['comment'];
                                             $rating->update($rating->fields);
                                             break;
                                          }
                                       }
                                    }
                                 }
                              }
                           }

                           break;
                        default:
                           if ($msg = $this->processMessage($this->data['text'])) {
                              /*if (strcasecmp($msg, 'help') == 0 || strcasecmp($msg, 'h') == 0) {
                                 $this->addTicket($uid, 'help');
                              }
                              elseif (strcasecmp($msg, 'daily') == 0 || strcasecmp($msg, 'd') == 0) {
                                 $this->addTicket($uid, 'daily report');
                              }
                              elseif (strcasecmp($msg, 'weekly') == 0 || strcasecmp($msg, 'w') == 0) {
                                 $this->addTicket($uid, 'weekly report');
                              }
                              elseif (strcasecmp($msg, 'monthly') == 0|| strcasecmp($msg, 'm') == 0) {
                                 $this->addTicket($uid, 'monthly report');
                              } else {
                                 $this->addTicket($uid, $msg);
                              }*/
                              $this->addTicket($uid, $msg);
                           }
                     }
                  }
                  break;
               case PluginRocketchatWebhook::ROOM_JOINED:
               case PluginRocketchatWebhook::ROOM_LEFT:
                  $input = [];
                  if ($item['itemtype'] == 'Ticket') {
                     $usrlnk = new Ticket_User();
                     $input['tickets_id'] = $item['items_id'];
                  }
                  if ($item['itemtype'] == 'Problem') {
                     $usrlnk = new Problem_User();
                     $input['problems_id'] = $item['items_id'];
                  }
                  if ($item['itemtype'] == 'Change') {
                     $usrlnk = new Change_User();
                     $input['changes_id'] = $item['items_id'];
                  }
                  if (!empty($input)) {
                     $input['users_id'] = $uid;
                     if ($result['event'] == PluginRocketchatWebhook::ROOM_JOINED) {
                        $crit = [];
                        foreach ($input as $key => $val) {
                           $crit[] = "`$key`=$val";
                        }
                        $condition = join(' AND ', $crit);
                        if (count($usrlnk->find($condition)) == 0) {
                           $input['type'] = $usrlnk::OBSERVER;
                           $usrlnk->add($input);
                        }
                     }
                     if ($result['event'] == PluginRocketchatWebhook::ROOM_LEFT) {
                        $input['type'] = $usrlnk::OBSERVER;
                        $usrlnk->deleteByCriteria($input);
                     }
                  }
                  break;
               case PluginRocketchatWebhook::FILE_UPLOADED:
                  $text = '';
                  if (!empty($this->data['message']->attachments[0]->title_link)) {
                     $url = $this->getChatClient()->server . $this->data['message']->attachments[0]->title_link;
                     $text .= sprintf(__("Uploaded a file:\n%s", 'rocketchat'), $url);
                     if (!empty($this->data['message']->attachments[0]->description)) {
                        $text .= "\n";
                        $text .= $this->data['message']->attachments[0]->description;
                     }
                  }
                  switch ($item['itemtype']) {
                     case 'Ticket':
                        $this->addTicketFollowup($uid, $item['items_id'], $text);
                        break;
                     case 'Problem':
                     case 'Change':
                        $this->loadSession($uid);
                        $this->addNotepad($uid, $item['itemtype'], $item['items_id'], $text);
                        break;
                  }
                  break;
            }
         }
      }
   }

   function addTicketFollowup($user_id, $ticket_id, $content = '', $approve = '') {
      $followup = new TicketFollowup();
      $input = [
         'users_id' => $user_id,
         'tickets_id' => $ticket_id,
         'content' => $content,
         'requesttypes_id' => $this->opt->requesttypes_id,
      ];
      if ($approve == '1') {
         $input['add_close'] = __('Approve the solution');
      }
      if ($approve == '0') {
         $input['add_reopen'] = __('Refuse the solution');
      }
      $followup->add($input);
   }

   function updateValidation($user_id, $item_id, $item_type, $comment = '', $approve = '') {
      switch ($approve) {
         case '1':
            $status = CommonITILValidation::ACCEPTED;
            break;
         case '0':
            $status = CommonITILValidation::REFUSED;
            break;
         default:
            $status = null;
            break;
      }
      if (isset($status)) {
         switch ($item_type) {
            case 'Ticket':
               $valid = new TicketValidation();
               $condition = "`users_id_validate` = '$user_id' AND `tickets_id` = '$item_id'";
               break;
            case 'Change':
               $valid = new ChangeValidation();
               $condition = "`users_id_validate` = '$user_id' AND `changes_id` = '$item_id'";
               break;
         }
         $condition .= " AND `status` IN (" . CommonITILValidation::WAITING . ", " . CommonITILValidation::NONE . ")";
         foreach ($valid->find($condition) as $item) {
            $item['status'] = $status;
            if (empty($comment)) {
               $item['comment_validation'] = '&nbsp;';
            } else {
               $item['comment_validation'] = $comment;
            }
            $valid->update($item);
         }
      }
   }

   function addNotepad($user_id, $item_type, $item_id, $text) {
      $note = new Notepad();
      $input = [
         'itemtype' => $item_type,
         'items_id' => $item_id,
         'users_id' => $user_id,
         'users_id_lastupdater' => $user_id,
         'content' => $text,
         'requesttypes_id' => $this->opt->requesttypes_id
      ];
      $note->add($input);
   }

   function addTicket($user_id, $content) {
      $ticket = new Ticket();
      $input = [
         'users_id_recipient' => $user_id,
         'users_id_lastupdater' => $user_id,
         '_users_id_requester' => $user_id,
         'name' => '',
         'content' => $content,
         '_tickettemplates_id' => $this->opt->tickettemplates_id,
         'requesttypes_id' => $this->opt->requesttypes_id,
      ];
      $ticket->add($input);
   }

   function loadSession($user_id) {
      $user = new User();
      $user->getFromDB($user_id);
      $session = 0;
      if (isset($_SESSION['glpiactive_entity'])) {
         $session = $_SESSION['glpiactive_entity'];
      }
      $user->loadMinimalSession($session, true);
   }

   function loadHooks() {
      $plug = new Plugin();
      $plug->init();
      $plug->doHook("init_session");
   }

   function processMessage($text) {
      $text = trim($text);
      if (preg_match('/^(\!(help|h))\s+(.*)/si', $text, $matches)) {
         $cmd = strtolower($matches[2]);
         $opt = $matches[3];
         if ($cmd == 'help' || $cmd == 'h') {
            return trim($opt);
         }
      }
      return false;
   }

   function explodeMesage($text) {
      if (preg_match('/^\s*(.)\s*(.*)/si', $text, $matches)) {
         if (isset($matches[1])) {
            $result['control'] = $matches[1];
            $result['comment'] = '';
            if (isset($matches[2])) {
               $result['comment'] = trim($matches[2]);
            }
            return $result;
         }
         return;
      }
   }

   function getBotID() {
      $cli = new PluginRocketchatClient();
      return $cli->get($cli::VAR_USER_ID);
   }

   function getChatClient() {
      $cli = new PluginRocketchatClient();
      return $cli->get($cli::VAR_BOT_ACCOUNT);
   }

}
