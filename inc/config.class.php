<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginRocketchatConfig extends PluginConfigVariable {

   const VAR_SETTINGS = 'settings';
   const PLUGIN       = 'rocketchat';

   //static $rightname      = 'plugin_rocketchat_config';
   protected $displaylist = false;

   static function getTypeName($nb = 0) {
      return PluginRocketchatTr::__('Настройки интеграции Rocket.Chat');
   }

   static function getMenuName() {
      return PluginRocketchatTr::__('Настройки Rocket.Chat');
   }

   function defaultValue($variable) {
      switch ($variable) {
         case self::VAR_SETTINGS:
            return [
               'requesttypes_id'    => 0,
               'tickettemplates_id' => 0
            ];
         default:
            return false;
      }
   }

   function showFormMain() {

      $val = $this->defaultValue(self::VAR_SETTINGS);
      if (!empty($this->fields['value'])) {
         $val = $this->fields['value'];
      }

      echo '<tr class="tab_bg_2">';
      echo '<td>' . __('Source of followup') . '</td>';
      echo '<td colspan="2">';
      echo '<input type="hidden" name="name" value="' . self::VAR_SETTINGS . '">';
      echo '<input type="hidden" name="plugin" value="' .  self::PLUGIN . '">';
      $params = [
         'name'  => 'value[requesttypes_id]',
         'value' => $val->requesttypes_id
      ];
      RequestType::dropdown($params);
      echo '</td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td>' . PluginRocketchatTr::__('Шаблон заявки') . '</td>';
      echo '<td colspan="2">';
      $params = [
         'name'  => 'value[tickettemplates_id]',
         'value' => $val->tickettemplates_id
      ];
      TicketTemplate::dropdown($params);
      echo '</td>';
      echo '</tr>';

   }

}
