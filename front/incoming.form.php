<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

include __DIR__ . '/../../../inc/includes.php';

// PHP Deprecated:  Automatically populating $HTTP_RAW_POST_DATA is deprecated and will be removed
// in a future version. To avoid this warning set 'always_populate_raw_post_data' to '-1' in php.ini
// and use the php://input stream instead. in Unknown on line 0
$data = file_get_contents('php://input');
$webhook = new PluginRocketchatIncoming();
$webhook->process($data);
