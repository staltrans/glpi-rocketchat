<?php

/**
 -------------------------------------------------------------------------
 RocketChat plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/rocketchat
 -------------------------------------------------------------------------

 LICENSE

 This file is part of RocketChat.

 RocketChat is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 RocketChat is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RocketChat. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

include __DIR__ . '/../../../inc/includes.php';

Session::checkLoginUser();

$webhook = new PluginRocketchatWebhook();

if (!empty($_POST)) {
   $webhook->save($_POST);
}

if (!isset($_GET['id'])) {
   $_GET['id'] = '';
}

if (!isset($_GET['withtemplate'])) {
   $_GET['withtemplate'] = "";
}

if ($webhook::canView()) {
   Html::header($webhook->getTypeName(Session::getPluralNumber()), $_SERVER['PHP_SELF'], 'plugins', 'pluginrocketchatwebhook');
   $webhook->display($_GET);
   Html::footer();
} else {
   Html::displayRightError();
}
