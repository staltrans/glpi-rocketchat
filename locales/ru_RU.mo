��    0      �  C         (     )  	   8     B     V     i     }     �  
   �  	   �     �     �     �     �               ,     ?  #   T      x     �     �     �     �     �  
   �     �          %     8     J     Y     l     �     �     �     �     �  
   �     �     �          /     H     c     w          �  �  �  $   b	     �	  &   �	      �	  "   �	  ,   �	  +   +
  )   W
  6   �
  -   �
     �
  0   �
     /  <   ?  :   |  (   �  *   �  @     0   L  :   }  9   �     �  %        7     F  ,   ^     �  /   �  +   �  !     $   )     N  0   c  )   �  
   �  6   �             @   4  B   u  @   �  <   �  <   6     s     �  Q   �      �            (   0                                               	           %                    !             *       ,   "          
       .   -       +          $          '   /                    )                         #   &          %s adds a note About bot All direct messages All private groups All public channels Archive group error Attached document Auth Token Auth info Authorization error on %s Bot Account Change description error Channel type Check client auth status Email verified Error change topic Error creating group Error getting information about bot Execution time from %1$s to %2$s Fetch users error Fetch users from rocket.chat Generate URL Generate token HTTP code %s Install %s Installed / not configured Misc Settings Please activate %s Please install %s Plugin options Rocket Chat Config Rocket.Chat webhooks Send message error Status connection Success This plugin requires %s Tickets template UTC offset Unarchive group error Update of a change by user Update of a problem by user Update of a task by user Update of a ticket by user Uploaded a file:
%s User ID User invite to group error Webhook for event: %s Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-02 16:40+0500
PO-Revision-Date: 2017-10-02 16:53+0500
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
Last-Translator: boris_t <boris@talovikov.ru>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru
 %s добавляет заметку О боте Все личные сообщения Все закрытые чаты Все публичные чаты Ошибка архивации группы Прикрепленный документ Токен аутентификатции Информация об аутентификации Ошибка авторизации для %s Аккаунт бота Ошибка изменения описания Тип чата Проверка статуса аутентификации Электронная почта подтверждена Ошибка изменения темы Ошибка создания группы Ошибка получения информации о боте Время выполнения c %1$s по %2$s Ошибка получения пользователей Получение пользователей rocket.chat Сгенерировать URL Сгенерировать токен HTTP код %s Установить %s Установлен / не настроен Прочие настройки Пожалуйста, активируйте %s Пожалуста, установите %s Настройки плагина Конфигурация Rocket Chat Rocket.Chat webhooks Ошибка отправки сообщения Состояние подключения Успех Для этого плагина требуется %s Шаблон заявки Смещение от UTC Ошибка извлечения из архива группы Обновление изменения пользователем Обновление проблемы пользователем Обновление задачи пользователем Обновление заявки пользователем Загружен файл:
%s ID пользователя Пригласить пользователя в группу не удалось Webhook для события %s 